<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test_Application extends Model
{
    protected $table = 'Test_Application';
    protected $primaryKey = 'AppId';
}
