<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dept extends Model
{
    protected $table = 'Dept';
    protected $primaryKey = 'DeptId';

    //
}
