<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Patient;
use App\Test_Application;
use App\User;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $test = Test::all();
        return view('home', ['test' => $test]);
    }

    public function viewPatients (Request $request){
        $patients = Patient::all();
        return view('viewPatients',['list' => $patients]);
    }

    public function viewPatientDetails($id){
        $patient = Patient::where('PatId',$id)->get()->first();
        $user = User::where('profileId',$id)->get()->first();

        return view('viewPatientDetails', ['patient' => $patient, 'user' => $user]);
    }

    public function postNewPatient(Request $request){

        $patient = new Patient();
        $patient->Fname = $request->input('Fname');
        $patient->Lname = $request->input('Lname');
        $patient->Gender = $request->input('Gender');
        $patient->Age = $request->input('Age');
        $patient->Weight = $request->input('Weight');
        $patient->BloodPressure = $request->input('BloodPressure');
        $patient->Contact = $request->input('Contact');
        $patient->Address = $request->input('Address');
        $patient->save();

        $patid = $patient->id;
        $user = new User();
        $user->name = $request->input('name');
        $user->password = Hash::make( $request->input('password'));
        $user->email = $request->input('email');
        $user->profileId = $patid;
        $user->save();


        return view('home', ['status' => 'Successful']);
    }


    public function postCreateTest(Request $request){

        $test = new Test();
        $test->TestName = $request->input('TestName');
        $test->Type = $request->input('Type');
        $test->Description = $request->input('Description');
        $test->Amount = $request->input('Amount');
        $test->save();

        return view('home', ['status' => 'Successfully Created Test']);

    }

    public function postApplyTest(Request $request){
        $testApp = new Test_Application();
        $testApp->PatId = $request->input('PatId');
        $testApp->TestId = $request->input('TestId');
        $testApp->save();

        return view('home',['status' => 'Successfully Applied for Test']);
    }

    public function postUpdatePatient (Request $request){
        $id = $request->input('id');
        $patient = Patient::where('PatId',$id)->get()->first();
        $patient->Fname = $request->input('Fname');
        $patient->Lname = $request->input('Lname');
        $patient->Gender = $request->input('Gender');
        $patient->Age = $request->input('Age');
        $patient->Weight = $request->input('Weight');
        $patient->BloodPressure = $request->input('BloodPressure');
        $patient->Contact = $request->input('Contact');
        $patient->Address = $request->input('Address');
        $patient->save();

        $user= User::where('profileId',$id)->get()->first();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return redirect('/patient/'.$id);
    }


}
