<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','ProfileId'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Patient(){
        return $this->hasOne('App\Patient','PatId','profileId');
    }

    public function Staff(){
        return $this->hasOne('App\Staff','StaffId','profileId');
    }
}
