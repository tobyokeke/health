<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'Staff';
    protected $primaryKey = 'StaffId';

    public function User(){
        return $this->belongsTo('App\User','StaffId','profileId');
    }

}
