<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'Payment';
    protected $primaryKey = 'PayId';

    public function User(){
        return $this->belongsToMany('App\Patient','PatId');
    }
    //
}
