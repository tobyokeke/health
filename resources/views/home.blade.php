@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                @if(isset($status))
                <p class="alert alert-success"> {{$status}}</p>
                @endif

                <br>

                @if(Auth::user()->role == 'Lab Attendant')
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Add Patient
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">

                                @if(Auth::user()->role == "Patient")
                                    <h1>Welcome {{Auth::user()->name}}</h1>
                                @endif
                                @if(Auth::user()->role == "Lab Attendant")

                                    <form class="form-group" method="post" action="{{url('/home')}}">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token">

                                        <label >Username:</label>
                                        <input type="text" class="form-control" name="name">

                                        <label >Email:</label>
                                        <input type="text" class="form-control" name="email">

                                        <label >Password:</label>
                                        <input type="text"  class="form-control" name="password">

                                        <label >First Name:</label>
                                        <input type="text" class="form-control" name="Fname">

                                        <label >Last Name:</label>
                                        <input type="text" class="form-control" name="Lname">

                                        <label >Gender:</label>
                                        <input type="text" class="form-control" name="Gender">

                                        <label >Age:</label>
                                        <input type="text" class="form-control" name="Age">

                                        <label >Weight:</label>
                                        <input type="text" class="form-control" name="Weight">

                                        <label >Blood Pressure:</label>
                                        <input type="text" class="form-control" name="BloodPressure">

                                        <label >Contact:</label>
                                        <input type="text" class="form-control" name="Contact">

                                        <label >Address:</label>
                                        <textarea class="form-control" name="Address"></textarea> <br>




                                        <button class="btn btn-primary" type="submit">Add Patient</button>
                                    </form>
                                @endif
                            </div>

                        </div>
                    </div>
                @endif

                @if(Auth::user()->role == 'Lab Technician')
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Create Test
                                </a>
                            </h4>
                        </div>

                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <form method="post" action="{{url('/test/create')}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                    <label>Name</label>
                                    <input type="text" class="form-control" name="TestName">

                                    <label>Type</label>
                                    <input type="text" class="form-control" name="Type">

                                    <label>Description</label>
                                    <input type="text" class="form-control" name="Description">

                                    <label>Amount</label>
                                    <input type="text" class="form-control" name="Amount"> <br>

                                    <button type="submit" class="btn btn-primary">Add</button>

                                </form>
                            </div>
                        </div>
                    </div>

                @endif

                @if(Auth::user()->role == 'Patient' && isset($test))
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Apply for Test
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <form method="post" action="{{url('/test/apply')}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="PatId" value="{{Auth::user()->profileId}}">
                                    <select name="TestId">
                                        @foreach($test as $item)
                                        <option value="{{$item->TestId}}">{{$item->TestName}} : {{$item->Amount}}</option>
                                        @endforeach
                                    </select>

                                    <button type="submit" class="btn btn-primary">Apply</button>
                                </form>


                            </div>
                        </div>
                    </div>
                @endif
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
