@extends('layouts.app')

@section('content')

    <div class="container">

        <h2 align="center">Update Patient Record</h2>

        @if(isset($patient) && isset($user))
        <div class="row">
            <div class="col-md-4" >
            <p style="font-size:24px;">
                Username: {{$user->name}} <br>
                Email: {{$user->email}} <br>
                First name: {{$patient->Fname}} <br>
                Surname: {{$patient->Lname}} <br>
                Gender:  {{$patient->Gender}} <br>
                Age:  {{$patient->Age}} <br>
                Wieght:  {{$patient->Weight}} <br>
                Blood Pressure:  {{$patient->BloodPressure}} <br>
                Contact:  {{$patient->Contact}} <br>
                Address:  {{$patient->Address}} <br>
                Date Added:  {{$patient->created_at}} <br>
                Date Last Updated:  {{$patient->updated_at}} <br>
            </p>
            </div>

            <div class="col-md-8">
            <form class="form-group" method="post" action="{{url('/patient/update')}}">
                <input type="hidden" value="{{csrf_token()}}" name="_token">
                <input type="hidden" value="{{$patient->PatId}}" name="id">

                <label >Username:</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}">

                <label >Email:</label>
                <input type="text" class="form-control" name="email" value="{{$user->email}}">

                <label >First Name:</label>
                <input type="text" class="form-control" name="Fname" value="{{$patient->Fname}}">

                <label >Last Name:</label>
                <input type="text" class="form-control" name="Lname" value="{{$patient->Lname}}">

                <label >Gender:</label>
                <input type="text" class="form-control" name="Gender" value="{{$patient->Gender}}">

                <label >Age:</label>
                <input type="text" class="form-control" name="Age" value="{{$patient->Age}}">

                <label >Weight:</label>
                <input type="text" class="form-control" name="Weight" value="{{$patient->Weight}}">

                <label >Blood Pressure:</label>
                <input type="text" class="form-control" name="BloodPressure" value="{{$patient->BloodPressure}}">

                <label >Contact:</label>
                <input type="text" class="form-control" name="Contact" value="{{$patient->Contact}}">

                <label >Address:</label>
                <textarea class="form-control" name="Address">{{$patient->Address}}</textarea> <br>




                <button class="btn btn-primary" type="submit">Update</button>
            </form>
            </div>
        @endif

        </div>
    </div>
@endsection